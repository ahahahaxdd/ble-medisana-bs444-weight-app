# BLE Medisana BS444 Weight App

Class Project to get started with Bluetooth. A simple Android App which scans for BluetoothLE Devices and connects to a BLE Scale.

## The following guides were leading for the implementation

[openScale](https://github.com/oliexdev/openScale/wiki/Medisana-BS444) \
[Shahar Avigezer](https://medium.com/@shahar_avigezer/bluetooth-low-energy-on-android-22bc7310387a) \
[Bluetooth Low Energy](https://developer.android.com/guide/topics/connectivity/bluetooth/ble-overview) \
[Stuart Kent](https://youtu.be/jDykHjn-4Ng) \
